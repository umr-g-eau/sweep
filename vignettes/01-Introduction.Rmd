---
title: "01-Introduction"
output: 
  rmarkdown::html_vignette:
    css: style.css
vignette: >
  %\VignetteIndexEntry{01-Introduction}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r setup, include=FALSE, file='setup.R'}
knitr::opts_chunk$set(echo = TRUE)
```

#Introduction

The Sweep package aims to facilitate the extraction of environmental data from various cartographic or spatial sources or APIs. It allows for the retrieval of this information using specific coordinates or spatial grids. This package also formats the extracted data in a way that makes it easily exploitable for further analysis.

```{r}
library("sweep")
```

