
# <img src="man/figures/logo_sweep_200px.png" alt="sweep" align="right" style="width:200px"/> SWEEP - Spatial Weather and Environmental data Extraction R Package support

<!-- badges: start -->

[![CRAN](https://www.r-pkg.org/badges/version-ago/sweep)](https://forgemia.inra.fr/umr-g-eau/sweep.git)
[![License:
GPL-3](https://img.shields.io/badge/license-AGPL--3-orange.svg)](https://cran.r-project.org/web/licenses/AGPL-3)
[![Dev check
status](https://forgemia.inra.fr/umr-g-eau/sweep/badges/main/pipeline.svg)](forgemia.inra.fr/umr-g-eau/sweep/-/pipelines)
<!-- badges: end -->

The Swwep package aims to facilitate the extraction of environmental
data from various cartographic or spatial sources or APIs. It allows for
the retrieval of this information using specific coordinates or spatial
grids. This package also formats the extracted data in a way that makes
it easily exploitable for further analysis.

## Installation

You can install the development version of sweep like so:

``` r
install.packages("remotes")
remotes::install_git("https://package_read:oyj8Yzbryvzw2iGyQQ-N@forgemia.inra.fr/umr-g-eau/sweep.git", build_vignettes = TRUE)
```

## Launch the library

``` r
library(sweep)
```

## Documentation

UMR G-EAU - Gestion de l’Eau, Acteurs, Usages (Montpellier)

<https://www.g-eau.fr/index.php/fr/>

Equipe OPTIMISTE - Optimisation du Pilotage et des Technologies
d’Irrigation, Minimisation des IntrantS, Transferts dans l’Environnement

<https://www.g-eau.fr/index.php/fr/umr-geau/les-equipes/item/410-optimisation-du-pilotage-et-des-technologies-d-irrigation-minimisation-des-intrants-transferts-environnementaux-optimiste>

<div style="display: flex; justify-content: space-between; align-items: center;">

<img src="man/figures/Logo-INRAE_Transparent.svg.png" alt="G-EAU" width="300"/><img src="man/figures/logo_G-EAU.jpg" alt="G-EAU" width="200"/>
